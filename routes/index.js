var express = require('express');
var router = express.Router();

/* GET home page. */
// router.get('/', function(req, res, next) {
//   res.render('index', { title: 'Express' });
// });
const books = require('./books')
const users = require('./users')
const authors = require('./authors')
const genre = require('./genre')

router.use('/books', books)
router.use('/users', users)
router.use('/authors', authors)
router.use('/genre', genre)

module.exports = router;
