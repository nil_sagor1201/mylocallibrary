const express = require('express')
const router = express.Router()

const { Book, Genre, Author } = require('../model')

/* GET users listing. */
/* GET book page.
@route /book/
@desc books list info get and create a new book
@access   public

*/
router.route('/')
  .get(async function(req, res, next) {
    const book = await Book.find({}, 'title author')
                            .populate('author')
    res.send(book)
  })
  .post(async function(req, res) {
    // if (!)
    const genres = await Genre.find()
    const authors = await Author.find()

    if (!(req.body.genre instanceof Array)) {
      if (typeof req.body.genre === 'undefined') {
        req.body.gerne = []
      } else {
        req.body.genre = new Array(req.body.genre)
      }

    }
    let results
    for (let i=0; i<results.genres.length; i++) {
      if (book.genre.indexOf(results.genres[i]._id) > -1) {
        results.genres[i].checked = 'true'
      }
    }
    const { title, author, summary, isbn, genre } = req.body
    const book = new Book({
      title,
      author,
      summary,
      isbn,
      genre
    })
    const bookRes = await book.save()
    res.status(201).send(bookRes)
  })

// /* GET author page.
// @route /author/:id
// @desc route for a single author info, update and delete 
// @access  PUBLIC

// */

// router.route('/:id')
//   .get(async (req,res) => {
//     const author = await Author.findById(req.params.id)
//     res.send(author)
//   })
//   .patch(async (req, res) => {
//     await Author.updateOne({_id:req.params.id}, {...req.body})
//     res.send("Got a update request" )
//   })
//   .delete(async (req, res) => {
//     await Author.deleteOne({ _id: req.params.id })
//     res.send("Got a delete request")
//   })


module.exports = router