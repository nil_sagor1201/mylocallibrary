const express = require('express')
const router = express.Router()

const { Author } = require('../model')

/* GET users listing. */
/* GET author page.
@route /author/
@desc author info get and create a new author
@access   public

*/
router.route('/')
  .get(async function(req, res, next) {
    const author = await Author.find()
    res.send(author)
  })
  .post(async function(req, res) {
    const { first_name, family_name, date_of_birth, date_of_death } = req.body
    const author = new Author({
      first_name,
      family_name,
      date_of_birth,
      date_of_death
    })
    const authorRes = await author.save()
    res.status(201).send(authorRes)
  })

/* GET author page.
@route /author/:id
@desc route for a single author info, update and delete 
@access  PUBLIC

*/

router.route('/:id')
  .get(async (req,res) => {
    const author = await Author.findById(req.params.id)
    res.send(author)
  })
  .patch(async (req, res) => {
    await Author.updateOne({_id:req.params.id}, {...req.body})
    res.send("Got a update request" )
  })
  .delete(async (req, res) => {
    await Author.deleteOne({ _id: req.params.id })
    res.send("Got a delete request")
  })


module.exports = router