const mongoose = require('mongoose')

const Schema = mongoose.Schema

const BookSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  author: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'Author'
  },
  summary: {
    type: String,
    required: true
  },
  isbn: {
    type: String,
    required: true
  },
  genre: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Gerne'
    }
  ]
})

// virtual Schema for book's URL
BookSchema
.virtual('url')
.get(function () {
  return '/book/' + this._id
})



const book = mongoose.model('MyBook', BookSchema)
module.exports = book