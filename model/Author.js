const mongoose = require('mongoose')

const Schema = mongoose.Schema

const AuthorSchema = new Schema({
  first_name: {
    type: String,
    required: true,
    maxlength: 100
  },
  family_name: {
    type: String,
    required: true,
    maxlength: 100
  },  
  date_of_birth: {
    type: Date,
    required: true
  },
  date_of_death: {
    type: Date,
    required: true
  }  
})

// virtual Schema for author's fullname
AuthorSchema
.virtual('name')
.get(function () {
  let fullname = ''
  if (this.first_name && this.family_name) {
    fullname = this.first_name + ',' + this.family_name
  }
  if (!this.first_name || this.family_name) {
    fullname = ''
  }
  return fullname = ''
})

// virtual Schema for book's URL
AuthorSchema
.virtual('url')
.get(function () {
  return '/book/' + this._id
})



const author = mongoose.model('Author', AuthorSchema)
module.exports = author