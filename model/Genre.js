const mongoose = require('mongoose')

const Schema = mongoose.Schema

const GenreSchema = new Schema({
  name: {
    type: String,
    required: true,
    min: 3,
    max: 100
  }
  
})

// virtual Schema for book's URL
GenreSchema
.virtual('url')
.get(function () {
  return '/genre/' + this._id
})



const genre = mongoose.model('Genre', GenreSchema)
module.exports = genre