const mongoose = require('mongoose')
const mongoDB = 'mongodb://localhost:27017/locallibrary'
mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true})
const db = mongoose.connection
db.on('error', console.error.bind(console, 'MongoDB connection error'))

const Book = require('./Book')
const Author = require('./Author')
const Genre = require('./Genre')
const BookInstance = require('./BookInstance')

module.exports = {
  Book,
  Author,
  Genre,
  BookInstance
}